var app = new Vue({
    el: '#app',
    data: {
        questions: ["a"],
        random: 0,
        answer: [],
        correction: false,
        success: false,
    },
    created: function () {
        this.updateQuestion().then(this.getQuestion);
    },
    computed: {
        question: function () {
            return this.questions[this.random]
        },
        inputType: function () {
            if (this.question.multiple === true) {
                return "checkbox"
            } else {
                return "radio"
            }
        }
    },
    methods: {
        updateQuestion: async function () {
            console.log("Updating question list...");
            await fetch("questions.json")
                .then(response => response.json())
                .then(json => { 
                    this.questions = json.questions; 
                    console.log(this.questions);
                })
                // .catch(error => console.log("error: " + error.message));
        },
        getQuestion: function () {
            console.log("Selecting a new question...");
            let random = Math.floor(Math.random() * this.questions.length);
            this.random = random;
            this.answer = [];
            this.correction = false;
        },
        checkAnswers: function () {
            this.correction = true;
            if (JSON.stringify(this.answer) === JSON.stringify(this.question.right_answer)) {
                this.success=true
            } else {
                this.success=false
            }
        },
        answerClass: function (answer_id) {
            if (this.correction) {
                if (typeof this.question.right_answer === "number" && answer_id === this.question.right_answer) {
                    return "is-valid"
                } else if (typeof this.question.right_answer === "object" && this.question.right_answer.includes(answer_id)) {
                    return "is-valid"
                } else {
                    return "is-invalid"
                }
            } else {
                return ""
            }
        }
    }
})