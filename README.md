# Quizz PSE
Quizz PSE est un projet de quizz à destination des titulaires des diplômes de Premiers Secours en Equipe de niveau 1 et 2 (PSE1 & PSE2), pour leur permettre d'approfondir et/ou revoir leur connaissance des recommandations techniques de la DGSCGC.

Pour contribuer, les plus téméraires peuvent directement modifier le fichier [questions.json](https://framagit.org/Rowin/quizz-pse/-/blob/master/questions.json) et faire une pull request. Vous pouvez aussi, si vous avez un compte, créer un ticket avec vos questions. Sinon, vous pouvez m'envoyer des propositions par mail ([gitlab-incoming+rowin-quizz-pse-66468-issue-@framagit.org](mailto:gitlab-incoming+rowin-quizz-pse-66468-issue-@framagit.org)). Dans tous les cas, les questions seront validées avant d'être intégrées.

Toutes les questions sont basées sur les dernières recommandations techniques du ministère de l'Intérieur et sont validées par un formateur PS à jour de FC.